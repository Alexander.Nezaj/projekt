Here's our logo (hover to see the title text):

Inline-style: 
![alt text]( Free-Logo-PNG3.png "Logo Title Text 1")

Reference-style: 
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"

Three or more...

---

Three or more...

***

| Tables        | Are           | Cool  | test  |
| ------------- |:-------------:| -----:| -----:|
| col 3 is      | right-aligned | $1600 |       |
| col 2 is      | centered      |   $12 |       |
| zebra stripes | are neat      |    $1 |       |

1. First item
2. Second item
3. Third item
4. Fourth item